// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){

    	if($('.top-banner .gallery')[0]){
    		$('.top-banner .gallery').slick({
			  arrows: false,
			  autoplay: true,
  			  autoplaySpeed: 7000,
  			  dots: true
			});
    	}

    	if($('.project-gallery')[0]){
    		$('.project-gallery').slick({
			  arrows: true
			});
    	}

    	$('.why-choose-us-content >.container').prepend($('.why-choose-us-title').html());
    	$('.why-choose-us-title').remove();

    	$('.service-gallery').slick({
    		centerMode: true,
    		centerPadding: '300px',
    		slidesToShow: 1,
    		initialSlide: 2,
    		responsive: [
    		{
		      breakpoint: 1024,
		      settings: {
		        centerMode: true,
	    		centerPadding: '150px',
	    		slidesToShow: 1,
	    		initialSlide: 2,
		      }
		    },
		    {
		      breakpoint: 576,
		      settings: {
		        centerMode: false,
	    		//centerPadding: '150px',
	    		slidesToShow: 1,
	    		initialSlide: 2,
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        centerMode: false,
	    		slidesToShow: 1,
	    		initialSlide: 1,
		      }
		    }
		  ]
    	});

    	$('.featured-gallery').masonry({
		  // set itemSelector so .grid-sizer is not used in layout
		  itemSelector: '.single-gallery',
		  // use element for option
		  columnWidth: '.grid-sizer',
		  percentPosition: true
		});

		$('.mobile-toggle').on('click', function(e){
			e.preventDefault();
			console.log('hrere');
			$('.mega-menu-toggle').toggleClass('mega-menu-open');
		});
        
    });

}(jQuery));