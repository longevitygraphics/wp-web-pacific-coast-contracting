<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content services">
			<main class="py-5">
				<div class="container text-center">
				<div class="h1 font-weight-bold mb-4 text-center">404</div>
					<div class="h4">Looks like the page you're looking for does not exist.</div>
					<div class="mt-5"><a class="btn btn-dark" href="/">BACK TO HOME</a></div>
				</div>
			</main>
		</div>
	</div>
	
<?php get_footer(); ?>