<?php

	switch ( get_row_layout()) {
		case 'services_slider':
			get_template_part('/layouts/layouts/services-slider');
		break;
		case 'featured_projects':
			get_template_part('/layouts/layouts/feature-projects');
		break;
		case 'featured_gallery':
			get_template_part('/layouts/layouts/feature-gallery');
		break;
	}

?>