<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

	<?php
		$view_all_button_active = get_sub_field('view_all_button_active');
	?>

	<div>
		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'project',
		    );

		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	
				<div class="featured-project pt-sm pb-sm row">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post(); 
		    	$title = get_the_title();
		    	$link = get_permalink();
		    	$image = get_the_post_thumbnail_url();
		    	$featured = get_field('featured');

		    	$terms= get_the_terms($post, 'project-category');
		    	?>
		    	
		    	<?php if($featured): ?>
			        <div class="col-12 col-sm-6 col-md-4 my-3">
			        	<img src="<?php echo $image; ?>" alt="">
			        	<a class="" href="<?php echo $link; ?>">
			        		<div class="overlay">
			        			<h4 class="text-white"><span class="mr-2">-</span>
									<?php if($terms && is_array($terms)): ?>
										<?php foreach ($terms as $key => $term): ?>
											<?php if ($key + 1 == sizeof($terms)): ?>
											  	<?php echo $term->name; ?>
											<?php else: ?>
												<?php echo $term->name; ?>,
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
			        			<span class="ml-2">-</span></h4>
			        			<h3 class="h1 text-white"><?php echo $title; ?></h3>
				        	</div>
				        </a>
			        </div>
			    <?php endif; ?>
				<?php
		        endwhile;
		        ?>
		        </div>
		    <?php endif; // End Loop

		    wp_reset_query();
		?>
	</div>
	
	<?php if($view_all_button_active == 1): ?>
		<div class="mt-5 text-center">
			<a href="/projects" class="btn btn-white">View All Projects</a>
		</div>
	<?php endif; ?>

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
