<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

	<?php
		$view_all_button_active = get_sub_field('view_all_button_active');
		include(locate_template('/templates/template-parts/page/feature-gallery.php')); 
	?>



<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
