<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

	<?php
		$view_all_button_active = get_sub_field('view_all_button_active');
		$service_categories = get_sub_field('service_categories');
		include(locate_template('/templates/template-parts/page/feature-service.php')); 
	?>

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
