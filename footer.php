<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>
	<footer id="site-footer" class="bg-dark">
		<div class="footer-wrapper container">
			<div class="row">
				<div class="footer-quick-links col-sm-6 col-md-2">
					<p class="footer-title">Quick Links</p>
					<?php wp_nav_menu( array( 'theme_location' => 'max_mega_menu_3' ) ); ?>
				</div>
				<div class="footer-contact col-sm-6 col-md-3">
					<p class="footer-title">Contact</p>
					<p><a href="tel:<?php echo do_shortcode(' [lg-phone-main]'); ?>"><?php echo do_shortcode(' [lg-phone-main]'); ?></a></p>
					<p><a href="mailto:<?php echo do_shortcode(' [lg-email]'); ?>"><?php echo do_shortcode(' [lg-email]'); ?></a></p>
				</div>
				<div class="col-12 col-md-3">
					<?php if(have_rows("footer_logos", "option")) : ?>
						<?php while(have_rows("footer_logos", "option")) : the_row(); ?>
							<?php 
								$image = get_sub_field('image','option');
								$link = get_sub_field('link', 'option');
								$link_url = $link['url'];
								$link_target = $link['target'] ? $link['target'] : '_self';
							 ?>
							 <a class="" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a>
						<?php endwhile; ?>
					<?php endif ?>
				</div>
				<div class="footer-ig col-12 col-md-4">
					<!-- <p class="footer-title">Connect with us</p>
					<div class="footer-social-media">
						<?php echo do_shortcode("[lg-social-media]"); ?>
						<hr />
					</div>
					<?php echo do_shortcode("[instagram-feed]"); ?> -->
					<p class="footer-title">Find Us</p>
					<div class="footer-map">
						<?php echo do_shortcode("[lg-map id=226]"); ?>
					</div>
				</div>
			</div>
			<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
				<div id="site-legal" class="d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
					<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
					<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
				</div>
			<?php endif; ?>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
