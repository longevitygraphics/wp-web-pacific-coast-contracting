<?php

	add_action('wp_content_top', 'featured_banner_top');
	add_action('wp_content_bottom', 'contact_bottom');

	function featured_banner_top(){
		ob_start(); ?>
			<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?> 
		<?php echo ob_get_clean();
	}

	function contact_bottom(){
		global $post;

		ob_start(); ?>
			<?php if($post->post_type = 'page' && !is_front_page() && !is_home()): ?>
				<?php get_template_part( '/templates/template-parts/footer/contact-section' ); ?> 
			<?php endif; ?>
		<?php echo ob_get_clean();
	}

	function main_nav_items ( $items, $args ) {
    if ($args->menu->slug == 'main-nav') {
	        $items .= '<li class="mega-menu-item"><a class="btn header-phone d-md-none d-lg-block" href="tel:+1'.do_shortcode('[lg-phone-main]').'"><span  itemprop="telephone"><i class="fas fa-phone mr-2"></i> '.format_phone(do_shortcode('[lg-phone-main]')).'</a></span></li>';
	    }
	    return $items;
	}
	add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

	add_action('restrict_manage_posts', 'service_filter_by_category');

	function service_filter_by_category() {
	  global $typenow;
	  $post_type = 'service'; // change to your post type
	  $taxonomy  = 'service-category'; // change to your taxonomy
	  if ($typenow == $post_type) {
	    $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
	    $info_taxonomy = get_taxonomy($taxonomy);
	    wp_dropdown_categories(array(
	      'show_option_all' => __("Show All {$info_taxonomy->label}"),
	      'taxonomy'        => $taxonomy,
	      'name'            => $taxonomy,
	      'orderby'         => 'name',
	      'selected'        => $selected,
	      'show_count'      => true,
	      'hide_empty'      => true,
	    ));
	  };
	}

	add_filter('parse_query', 'service_convert_id_to_term_in_query');
	function service_convert_id_to_term_in_query($query) {
	  global $pagenow;
	  $post_type = 'service'; // change to your post type
	  $taxonomy  = 'service-category'; // change to your taxonomy
	  $q_vars    = &$query->query_vars;
	  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
	    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
	    $q_vars[$taxonomy] = $term->slug;
	  }
	}

	// Add our custom permastructures for custom taxonomy and post
	add_action( 'wp_loaded', 'add_clinic_permastructure' );
	function add_clinic_permastructure() {
	  global $wp_rewrite;
	  add_permastruct( 'service', 'services/%service-category%/%service%', false );
	}
	// Make sure that all links on the site, include the related texonomy terms
	add_filter( 'post_type_link', 'service_permalinks', 10, 2 );
	function service_permalinks( $permalink, $post ) {
	  if ( $post->post_type !== 'service' )
	    return $permalink;
	  $terms = get_the_terms( $post->ID, 'service-category' );
	  
	  if ( ! $terms )
	    return str_replace( '%service-category%/', '', $permalink );
	  $post_terms = array();
	  foreach ( $terms as $term )
	    $post_terms[] = $term->slug;
	  return str_replace( '%service-category%', implode( ',', $post_terms ) , $permalink );
	}
?>