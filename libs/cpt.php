<?php

    function lg_custom_post_type(){
      register_post_type( 'service',
          array(
            'labels' => array(
              'name' => __( 'Services' ),
              'singular_name' => __( 'Service' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );

      register_post_type( 'project',
          array(
            'labels' => array(
              'name' => __( 'Projects' ),
              'singular_name' => __( 'Project' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );
    }

    add_action( 'init', 'lg_custom_post_type' );

    function lg_custom_taxonomy(){

      register_taxonomy(
        'service-category',
        'service',
        array(
          'label' => __( 'Category' ),
          'rewrite' => array( 'slug' => 'service-category' ),
          'hierarchical' => true,
        )
      );

      register_taxonomy(
        'project-category',
        'project',
        array(
          'label' => __( 'Category' ),
          'rewrite' => array( 'slug' => 'project-category' ),
          'hierarchical' => true,
        )
      );
    }

    add_action( 'init', 'lg_custom_taxonomy' );

?>