<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content project">
			<main>
				
				<div class="pt-5 center">
					<div class="container">
						<div class="site-gallery">
							
						</div>
						<div class="text-center mt-5 py-5">
							<a class="btn btn-dark site-gallery-learn-more" href="javascript:void(0);" href="">Load More</a>
						</div>
					</div>
				</div>

			</main>
		</div>
	</div>

	<div class="gallery-modal-wrap">
		<div class="gallery-modal">
			<a class="gallery-close">X</a>
			<div class="images"></div>
		</div>
	</div>

	<script>
		(function($) {
		    $(document).ready(function(){

		    	var index = 0;
		    	var items_per_load = 8;
		        <?php $images = get_field('gallery') ?>
		        var images = <?php echo json_encode($images); ?>;
		        
		        gallery_load();

		        $('.site-gallery-learn-more').on('click', function(){
		        	console.log('heiwew');
		        	gallery_load();
		        });

		        $('.gallery-modal-wrap, .gallery-close').on('click', function(){
		        	$('.gallery-modal-wrap').fadeOut();
		        	$('.gallery-modal .images').empty();
		        });

		        $('.gallery-modal').on('click', function(e){
		        	e.stopPropagation();
		        });

		        $('.site-gallery').on('click', '.gallery-image', function(){
		        	var src = $(this)[0].src;
		        	$('.gallery-modal .images').append('<img src="'+src+'">').closest('.gallery-modal-wrap').css("display", "flex").hide().fadeIn();
		        });

		        function gallery_load(){
		        	var element = $('.site-gallery');

		        	for(var i = index; i < index + items_per_load; i++){
		        		if(images[i]){
		        			element.append('<div><img class="gallery-image" src="'+ images[i].url +'"></div>');
		        		}else{
				        	$('.site-gallery-learn-more').hide();
				        	break;
		        		}
		        	}

		        	index += items_per_load;

		        }

		    });
		}(jQuery));
	</script>

<?php get_footer(); ?>