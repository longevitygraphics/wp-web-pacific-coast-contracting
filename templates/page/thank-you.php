<?php
/**
 * Template for Thank you Page
 */

get_header(); ?>

	<div id="primary" class="full-page thank-you">
		<div id="content" role="main">

			<div class="py-5 text-center">
				<div class="container">
					<div class="h1 font-weight-bold mb-4">THANK YOU</div>
					<div class="h4">One of our team members will get back to you shortly.</div>
					<div class="mt-5"><a class="btn btn-dark" href="<?php echo get_site_url(); ?>">BACK TO HOME</a></div>
				</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>