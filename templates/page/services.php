<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content services">
			<main class="py-5">

				<?php
					$intro_section = get_field('intro_section');
					$residential_intro = get_field('residential_intro');
					$residential_intro_image = get_field('residential_intro_image');
					$cta = get_field('cta');
					$commercial_intro = get_field('commercial_intro');
					$commercial_intro_image = get_field('commercial_intro_image');
				?>
				
				<!-- Intro -->
				<div class="py-5 center">
					<div class="container container-sm text-center">
						<h2 class="h1 font-weight-bold">Services</h2>
						<?php echo $intro_section; ?>
					</div>
				</div>
				<!-- end Intro -->

				<!-- Residential Intro -->
				<div class="py-5">
					<div class="row align-items-center no-gutters">
						<div class="col-md-6">
							<img class="img-fit services-intro-image" src="<?php echo $residential_intro_image['url']; ?>" alt="<?php echo $residential_intro_image['alt']; ?>">
						</div>
						<div class="col-md-6 px-3 px-md-5 py-4 py-md-0">
							<?php echo $residential_intro; ?>
						</div>
					</div>
				</div>
				<!-- end Residential Intro -->

				<!-- Residential Services -->
				<div class="py-5 container">
					<?php
						$args = array(
							'post_type' => 'service',
							'showposts'	=> -1,
							'tax_query' => array(
								array(
									'taxonomy' => 'service-category',
									'field'    => 'slug',
									'terms'    => 'residential',
								),
							),
						);

					    $result = new WP_Query( $args );

					    // Loop
					    if ( $result->have_posts() ) : ?>
					    	<div class="service-list">
					        <?php while( $result->have_posts() ) : $result->the_post(); 
					        	$title = get_the_title();
					        	$icon = get_field('icon');
					        	$short_description = get_field('short_description');
					        	$long_description = get_field('long_description');
						    ?>
						    	<div>
						    		<div class="service-title d-flex align-items-center">
						    			<span class="service-icon mr-3 d-inline-flex align-items-center"><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>"></span> <span><?php echo $title; ?></span>
						    		</div>
						    		<div class="service-short-description">
							    		<?php if($long_description) : ?>
							    			<p class="ml-5 mb-3"><?php echo short_string($long_description, 20); ?> ...</p>
							    			<a class="ml-5 text-capitalize btn btn-white" href="<?php echo get_the_permalink(); ?>">learn more</a>
							    		<?php endif; ?>
						    		</div>
						    	</div>
							<?php
					        endwhile; ?>
					        </div>
					    <?php endif; // End Loop

					    wp_reset_query();
					?>
				</div>
				<!-- end Residential Services -->

				<!-- Residential Gallery -->
				<div class="py-5 mb-5 container">
					<?php
						$view_all_button_active = -1;
						$service_categories = get_term_by('slug', 'residential', 'service-category')->term_id;
						include(locate_template('/templates/template-parts/page/feature-service.php')); 
					?>
				</div>
				<!-- end Residential Gallery -->

				<!-- CTA -->
				<div class="px-3 px-md-5 d-flex bg-dark flex-wrap text-white py-5 align-items-center justify-content-between">
					<div>
						<h2 class="mb-0 font-weight-bold text-white"><?php echo $cta['copy']; ?></h2>
					</div>
					<div>
						<a href="<?php echo $cta['button']['url']; ?>" class="btn btn-white"><?php echo $cta['button']['title']; ?></a>
					</div>
				</div>
				<!-- end CTA -->

				<!-- Commercial Intro -->
				<div class="py-5">
					<div class="row align-items-center no-gutters">
						<div class="col-md-6 px-3 px-md-5 order-2 order-md-initial py-4 py-md-0">
							<?php echo $commercial_intro; ?>
						</div>
						<div class="col-md-6 order-1 order-md-initial">
							<img class="img-fit services-intro-image" src="<?php echo $commercial_intro_image['url']; ?>" alt="<?php echo $commercial_intro_image['alt']; ?>">
						</div>
					</div>
				</div>
				<!-- end Commercial Intro -->

				<!-- Commercial Services -->
				<div class="py-5 container">
					<?php
						$args = array(
							'post_type' => 'service',
							'showposts'	=> -1,
							'tax_query' => array(
								array(
									'taxonomy' => 'service-category',
									'field'    => 'slug',
									'terms'    => 'commercial',
								),
							),
						);

					    $result = new WP_Query( $args );

					    // Loop
					    if ( $result->have_posts() ) : ?>
					    	<div class="service-list">
					        <?php while( $result->have_posts() ) : $result->the_post(); 
					        	$title = get_the_title();
					        	$icon = get_field('icon');
					        	$short_description = get_field('short_description');
					        	$long_description = get_field('long_description');
						    ?>
						    	<div>
						    		<div class="service-title d-flex align-items-center">
						    			<span class="service-icon mr-3 d-inline-flex align-items-center"><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>"></span> <span><?php echo $title; ?></span>
						    		</div>
						    		<div class="service-short-description">
						    			<?php if($long_description) : ?>
							    			<p class="ml-5 mb-3"><?php echo short_string($long_description, 20); ?> ...</p>
							    			<a class="ml-5 text-capitalize btn btn-white btn-sm" href="<?php echo get_the_permalink(); ?>">learn more</a>
							    		<?php endif; ?>
						    		</div>
						    	</div>
							<?php
					        endwhile; ?>
					        </div>
					    <?php endif; // End Loop

					    wp_reset_query();
					?>
				</div>
				<!-- end Commercial Services -->

				<!-- Commercial Gallery -->
				<div class="py-5 container">
					<?php
						$view_all_button_active = -1;
						$service_categories = get_term_by('slug', 'commercial', 'service-category')->term_id;
						include(locate_template('/templates/template-parts/page/feature-service.php')); 
					?>
				</div>
				<!-- end Commercial Gallery -->

			</main>
		</div>
	</div>

	<script>
		(function($) {
    		$(document).ready(function(){
				$('.service-list .service-title').on('click', function(){
					if($(this).parent().hasClass('active')){
						$(this).parent().removeClass('active').find('.service-short-description').fadeOut();
					}else{
						$(this).siblings('.service-short-description').fadeIn().parent().addClass('active').siblings().removeClass('active').find('.service-short-description').fadeOut();
					}
		    	});
		    });
		}(jQuery));
	</script>

<?php get_footer(); ?>