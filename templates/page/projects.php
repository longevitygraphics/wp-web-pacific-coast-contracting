<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content project">
			<main>
				
				<div class="pt-5 center">
					<div class="container">
						<?php get_template_part("/templates/template-parts/page/projects-filter"); ?>
						<?php get_template_part("/templates/template-parts/page/projects"); ?>
					</div>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>