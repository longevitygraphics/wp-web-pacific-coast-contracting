<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content project">
			<main>

				<?php 

					$terms = get_the_terms($post, 'project-category'); 
					$long_description = get_field('long_description');
					$gallery = get_field('gallery');
					$location = get_field('location');

					foreach ($terms as $key => $term) {
						if($key == 0){
							$terms_string .= $term->name;
						}else{
							$terms_string .= ' & ' . $term->name;
						}
					}

				?>
				
				<div class="pt-5 center">
					<div class="container py-5">
						<div class="row">
							<div class="col-12 col-md-6 order-2 order-md-initial px-3 px-md-5 py-4 py-md-0">
								<h2 class="mb-1"><?php echo $terms_string; ?> - <?php the_title(); ?></h2>
								<?php if($location): ?>
								<div class="text-gray"><?php echo $location; ?></div>
								<?php endif; ?>
								<div class="mt-4"><?php echo $long_description; ?></div>
							</div>
							<div class="col-12 col-md-6 order-1 order-md-initial">
								<?php 

								$images = get_field('gallery');

								if( $images ): ?>
								    <div class="project-gallery">
								        <?php foreach( $images as $image ): ?>
								            <div>
								            	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-full">
								            </div>
								        <?php endforeach; ?>
								    </div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>

				<?php
					$view_all_button_active = 1; 
				?>

				<div class="py-5 container">
					<div class="text-center mb-5">
						<h2 class="h1">Gallery</h2>
						<p>Features images for all our projects</p>
					</div>

					<?php include(locate_template('/templates/template-parts/page/feature-gallery.php')); ?>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>