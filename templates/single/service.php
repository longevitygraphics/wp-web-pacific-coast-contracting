<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content project">
			<main>

				<?php
					$long_description = get_field('long_description');
					$intro_image = get_field('intro_image');
					$terms = get_the_terms($post, 'service-category');
					$terms_string = '';

					if($terms && is_array($terms)){
						foreach ($terms as $key => $term) {
							if($key == 0){
								$terms_string .= $term->name;
							}else{
								$terms_string .= ' && ' . $term->name;
							}
						}
					}
				?>

				<div class="container py-5">
					<div class="row">
						<div class="col-md-6 order-2 order-md-initial pt-4 pt-md-0">
							<h2 class="font-weight-bold mb-2"><?php echo get_the_title(); ?></h2>
							<!-- <h3><?php echo $terms_string; ?></h3> -->

							<div class="pt-4">
								<?php echo $long_description; ?>
							</div>
						</div>
						<div class="col-md-6 order-1 order-md-initial">
							<img src="<?php echo $intro_image['url']; ?>" alt="<?php echo $intro_image['alt']; ?>">
						</div>
					</div>
				</div>

				<!-- Specialties -->
					<?php
					if( have_rows('specialties') ): ?>
						<div class="bg-gray-light">
							<div class="container specialties py-5">
								<h2 class="font-weight-bold text-center mb-3">Our Specialties</h2>
								<div class="row">
								    <?php while ( have_rows('specialties') ) : the_row();
								        $image = get_sub_field('image');
								        $title = get_sub_field('content')['title'];
								        $description = get_sub_field('content')['description'];
								        $slug = sanitize_title($title);
								    ?>
									<div class="col-12 col-md-6 col-lg-4 single-specialty my-4" id="<?php echo $slug; ?>">
										<div>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
											<div class="description py-4">
												<h3 class="font-weight-bold"><?php echo $title; ?></h3>
												<div><?php echo $description; ?></div>
											</div>
										</div>
									</div>
								    <?php endwhile; ?>
							    </div>
							</div>
						</div>
					<?php else :
					    // no rows found
					endif; 
					?>
				</div>
				<!-- end Specialties -->

				<!-- Gallery -->
				<div class="py-5">
					<div class="container text-center">
						<h2 class="font-weight-bold">Gallery</h2>
						<p>Features images for all our services</p>

						<?php
							$view_all_button_active = 1;
							include(locate_template('/templates/template-parts/page/service-gallery.php'));
						?>
					</div>
				</div>
				<!-- end Gallery -->

			</main>
		</div>
	</div>

	<script>
		// Windows Ready Handler
		 	
		(function($) {

			function getUrlVars() {
			    var vars = {};
			    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			        vars[key] = value;
			    });
			    return vars;
			}
			
		    $(document).ready(function(){

		    	var para = getUrlVars()["specialties"];
		    	var element = $('#'+para);
				var settings = {
			        duration: 2 * 1000,
			        offset: -80
			    };

			    if(para && element[0]){
			    	var scrollTo = element;
			    	KCollection.headerScrollTo(scrollTo, settings, function(){
			    		console.log('here now');
						if(!element.hasClass('service-animated')){
							element.addClass('animated shake active');
						}
			    	});
			    }
		        
		    });

		}(jQuery));
	</script>

<?php get_footer(); ?>