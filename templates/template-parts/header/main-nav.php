<div class="main-navigation d-none d-md-block">
  	<nav class="navbar navbar-default">  
  	    <!-- Brand and toggle get grouped for better mobile display -->
  	    <div class="navbar-header d-none">
  	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
  	        <span class="sr-only">Toggle navigation</span>
  	        <span class="icon-bar"></span>
  	        <span class="icon-bar"></span>
  	        <span class="icon-bar"></span>
  	      </button>
  	    </div>


        <!-- Main Menu  -->
        <?php 

          $mainMenu = array(
          	// 'menu'              => 'menu-1',
          	'theme_location'    => 'top-nav',
          	'depth'             => 2,
          	'container'         => 'div',
          	'container_class'   => 'collapse navbar-collapse',
          	'container_id'      => 'main-navbar',
          	'menu_class'        => 'nav navbar-nav',
          	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
          	'walker'            => new WP_Bootstrap_Navwalker()
          );
          wp_nav_menu($mainMenu);

        ?>
  	</nav>
</div>