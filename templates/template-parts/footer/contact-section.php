<?php if (!is_page("thank-you")): ?>
<section class="flexible-content contact-us <?php if($post->post_name != 'contact-us'): ?>bg-gray-light<?php endif; ?>">
	<div class="container  pt-5 pb-5 ">
				
<!--------------------------------------------------------------------------------------------------------------------------------->

	<?php $contact_copy = get_field('contact_copy', 'option'); ?>

	<div class="d-flex flexible_grid  row align-items-stretch justify-content-start">

		<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="grid-2-column order-md-initial col-12 col-md-7 order-2">
			
            <div class="text-editor">
            <?php echo $contact_copy; ?>
            <p><strong>Email:</strong></p>
            <a class="text-dark" href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a>
            <p>&nbsp;</p>
            <p><strong>Phone:</strong></p>
            <a class="text-dark" href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a>
            <p>&nbsp;</p>
            <p><strong>Address:</strong></p>
            <p><?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?></p>
            </div>
		</div>
		
		<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="grid-2-column order-md-initial col-12 col-md-5 order-2">
				
            <div class="text-editor">
            	<?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true tabindex=49]'); ?>
            </div>
		</div>
		
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

	</div>
</section>
<?php endif ?>