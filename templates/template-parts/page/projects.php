<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'project',
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
		<div class="portfolios py-5">
            <div class="grid-sizer"></div>
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
    	$main_image = get_the_post_thumbnail_url();
        $thumbnail_id = get_post_thumbnail_id( $post->ID );
        $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);   
    	$url = get_permalink();
    	$terms = get_the_terms($post->ID, 'project-category');
    	$categories = [];
        $short_description = get_field('short_description');

        if($terms && is_array($terms)){
        	foreach ($terms as $key => $value) {
        		array_push($categories, $value->slug);
        	}
        }

    	$categories = join(" ", $categories);
    ?>
    	
        <div class="single-portfolio <?php if($categories){ echo '' . $categories; } ?> all">
        	<div>
                <div class="image">
                     <a href="<?php echo $url; ?>"><img class="main" src="<?php echo $main_image; ?>" alt="<?php echo $alt; ?>"></a>
                    <div class="overlay">
                        <div><?php echo $short_description; ?> </div>
                    </div>   
                </div>
                <div class="title py-3 text-center">
                    <a class="text-dark" href="<?php echo $url; ?>"><?php the_title(); ?></a>
                </div>   
            </div>
        </div>

		<?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop

    wp_reset_query();
?>

<script>
(function($) {

    var default_hash = 'all';

    $(window).on('load', function(){
        var content = $('.portfolios');
        $mas = $('.portfolios').masonry({
            // options
            columnWidth: '.grid-sizer',
            itemSelector : '.single-portfolio',
        });

        $(window).on('hashchange', function () {

            if(location.hash == '' || !$('.filter[slug="'+ location.hash.substring(1) +'"]')[0]){
                location.hash = default_hash;
            }

            $('.filter[slug="'+ location.hash.substring(1) +'"]').addClass('active').siblings().removeClass('active');
            //content.find('>div').fadeOut(100, function(){
                //show_animate(content.find('.'+location.hash.substring(1)));
            //});
            content.find('>div').removeClass('single-portfolio');
            content.find('.'+location.hash.substring(1)).addClass('single-portfolio');
            $mas.masonry();

        }).trigger('hashchange');
    });

    $(document).ready(function(){

        var filter = $('.tab-filter-a');
        var content = $('.portfolios');
        
        
        //Project Filter
        filter.find('.filter').on('click', function(){
            location.hash = $(this).attr('slug');
        });

    });

}(jQuery));
</script>