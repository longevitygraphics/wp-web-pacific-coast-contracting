<div class="top-banner">
	<?php $top_banner = get_field('top_banner', 'option'); ?>

	<?php
	$images = get_field('top_banner');
	$size = 'full'; // (thumbnail, medium, large, full or custom size)

	if( $images ): ?>
	    <div class="gallery">
	        <?php foreach( $images as $image ): ?>
	            <div>
	            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
	            </div>
	        <?php endforeach; ?>
	    </div>
	<?php else: ?>

		<div class="default_banner">
			<?php 
				if($post->post_type == 'page'): 
				$page_default_banner = get_field('page_default_banner', 'option');
			?>
				<?php if($page_default_banner): ?>
					<img src="<?php echo $page_default_banner['url']; ?>" alt="<?php echo $page_default_banner['alt']; ?>">
				<?php endif; ?>
					
			<?php elseif($post->post_type == 'post'): 
				$blog_default_banner = get_field('blog_default_banner', 'option');
			?>

				<?php if($blog_default_banner): ?>
					<img src="<?php echo $blog_default_banner['url']; ?>" alt="<?php echo $blog_default_banner['alt']; ?>">
				<?php endif; ?>

			<?php elseif($post->post_type == 'service'): 
				$service_default_banner = get_field('service_default_banner', 'option');
				$commercial_default_banner = get_field('commercial_default_banner', 'option');
				$residential_default_banner = get_field('residential_default_banner', 'option');
				$terms = get_the_terms( $post->ID, 'service-category' );
				if ( !empty( $terms ) ) {
					$term = array_shift( $terms );
				}
			?>
				<?php if($commercial_default_banner && $term->slug == 'commercial'): ?>
					<img src="<?php echo $commercial_default_banner['url']; ?>" alt="<?php echo $commercial_default_banner['alt']; ?>">
				<?php endif; ?>

				<?php if($residential_default_banner && $term->slug == 'residential'): ?>
					<img src="<?php echo $residential_default_banner['url']; ?>" alt="<?php echo $residential_default_banner['alt']; ?>">
				<?php endif; ?>

			<?php elseif($post->post_type == 'project'): 
				$project_default_banner = get_field('project_default_banner', 'option');
			?>

				<?php if($project_default_banner): ?>
					<img src="<?php echo $project_default_banner['url']; ?>" alt="<?php echo $project_default_banner['alt']; ?>">
				<?php endif; ?>

			<?php endif; ?>
		</div>

	<?php endif; ?>


	<?php
		$text_overlay = get_field('text_overlay');
		$form_active = get_field('form_active');
		$form_overlay = get_field('form_overlay', 'option');
		$service_default_overlay = get_field('service_default_overlay','option');
	?>
	<div class="overlay">
		<div>
			<?php if ($text_overlay ): ?>
			<?php echo $text_overlay; ?>
			<?php elseif( $post->post_type == 'service' && $service_default_overlay): ?>
			<?php echo $service_default_overlay; ?>
			<?php endif ?>

		</div>
		<?php if($form_active == 1): ?>
		<div>
			<?php echo do_shortcode($form_overlay); ?>
		</div>
		<?php endif; ?>
	</div>
</div>