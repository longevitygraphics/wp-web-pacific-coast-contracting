	<div>
		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'service',
		        'tax_query' => array(
					array(
						'taxonomy' => 'service-category',
						'field'    => 'term_id',
						'terms'    => $service_categories
					),
				),
		    );

		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	
				<div class="service-gallery pt-sm pb-sm">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post(); 
		    	$title = get_the_title();
		    	$link = get_permalink();
		    	$image = get_the_post_thumbnail_url();
		    	?>
		    	
		        <div>
		        	<img src="<?php echo $image; ?>" alt="<?php esc_html ( get_the_post_thumbnail_caption() );  ?>">
		        	<div class="overlay text-center">
			        	<h3 class="h1 text-white"><?php echo $title; ?></h3>
			        	<a class="btn btn-white-transparent mt-3" href="<?php echo $link; ?>">Learn More</a>
		        	</div>
		        </div>

				<?php
		        endwhile;
		        ?>
		        </div>
		    <?php endif; // End Loop

		    wp_reset_query();
		?>
	</div>
	
	<?php if($view_all_button_active == 1): ?>
		<div class="mt-5 text-center">
			<a href="/services" class="btn btn-white">View All Services</a>
		</div>
	<?php endif; ?>