<?php 
	$terms = get_the_terms( $post->ID, 'service-category' );
	if ( !empty( $terms ) ) : 
?>
<?php 
	// get the first term
	$term = array_shift( $terms );
?>

	<?php if ($term->slug == 'residential'): ?>
		<?php
			if( have_rows('residential_gallery', 'option') ): ?>
				<div class="featured-gallery">
					<div class="grid-sizer"></div>
			    <?php while ( have_rows('residential_gallery', 'option') ) : the_row();
			        $image = get_sub_field('image');
			        $width = get_sub_field('width');
			        $height = get_sub_field('height');
			    ?>
					<div class="single-gallery <?php echo $height; ?> <?php echo $width; ?>">
						<img class="img-fit" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					</div>
			        <?php endwhile; ?>
			    </div>
			    <?php
					else :
					    // no rows found
					endif;
				?>
			<?php if($view_all_button_active == 1): ?>
				<div class="mt-5 text-center">
					<a href="/gallery" class="btn btn-white">View Full Gallery</a>
				</div>
			<?php endif; ?>
	<?php endif ?>
	<?php if ($term->slug == 'commercial'): ?>
			<?php
			if( have_rows('commercial_gallery', 'option') ): ?>
				<div class="featured-gallery">
					<div class="grid-sizer"></div>
			    <?php while ( have_rows('commercial_gallery', 'option') ) : the_row();
			        $image = get_sub_field('image');
			        $width = get_sub_field('width');
			        $height = get_sub_field('height');
			    ?>
					<div class="single-gallery <?php echo $height; ?> <?php echo $width; ?>">
						<img class="img-fit" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					</div>
			        <?php endwhile; ?>
			    </div>
			    <?php
					else :
					    // no rows found
					endif;
				?>
			<?php if($view_all_button_active == 1): ?>
				<div class="mt-5 text-center">
					<a href="/gallery" class="btn btn-white">View Full Gallery</a>
				</div>
			<?php endif; ?>
	<?php endif ?>

<?php endif; //end of of terms is not empty ?>