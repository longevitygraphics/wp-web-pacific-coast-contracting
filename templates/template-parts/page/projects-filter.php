<?php

$terms = get_terms( array(
    'taxonomy' => 'project-category',
    'hide_empty' => false,
) );

?>

<?php if($terms && is_array($terms)): ?>
	<div class="tab-filter-a">
		<div class="filter" slug="all">
			All Projects
		</div>
		<?php foreach ($terms as $key => $value): ?>
			<div class="filter" slug="<?php echo $value->slug; ?>">
				<?php echo $value->name; ?>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>